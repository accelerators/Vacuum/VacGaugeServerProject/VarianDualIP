#=============================================================================
#
# file :        Makefile
#
# description : Makefile to generate a TANGO device server.
#
# project :     VarianDual
#
# $Author:  $
#
# $Revision:  $
# $Date:  $
#
#=============================================================================
#                This file is generated by POGO
#        (Program Obviously used to Generate tango Object)
#=============================================================================
#
#
MAKE_ENV = /segfs/tango/cppserver/env

#=============================================================================
# PACKAGE_NAME is the name of the library/device/exe you want to build
#
PACKAGE_NAME = VarianDual
MAJOR_VERS   = 1
MINOR_VERS   = 0
RELEASE      = Release_$(MAJOR_VERS)_$(MINOR_VERS)

# #=============================================================================
# # RELEASE_TYPE
# # - DEBUG     : debug symbols - no optimization
# # - OPTIMIZED : no debug symbols - optimization level set to O2
# #-----------------------------------------------------------------------------
RELEASE_TYPE = DEBUG

#=============================================================================
# OUTPUT_TYPE can be one of the following :
#   - 'STATIC_LIB' for a static library (.a)
#   - 'SHARED_LIB' for a dynamic library (.so)
#   - 'DEVICE' for a device server (will automatically include and link
#            with Tango dependencies)
#   - 'SIMPLE_EXE' for an executable with no dependency (for exemple the test tool
#                of a library with no Tango dependencies)
#
OUTPUT_TYPE = DEVICE

#=============================================================================
# OUTPUT_DIR  is the directory which contains the build result.
# if not set, the standard location is :
#	- $HOME/DeviceServers if OUTPUT_TYPE is DEVICE
#	- ../bin for others
#
OUTPUT_DIR = ./bin/$(BIN_DIR)

#=============================================================================
# Tango Class list used by project
#
SERIAL_CLASS = Serial
SERIAL_HOME  = /segfs/tango/cppserver/protocols/SerialLine/src

VARIANDUALCTRL_CLASS = VarianDualCtrl
VARIANDUALCTRL_HOME  = /segfs/tango/cppserver/vacuum/ionpump/VarianDualCtrl

VARIANDUALIP_CLASS = VarianDualIP
VARIANDUALIP_HOME  = /segfs/tango/cppserver/vacuum/ionpump/VarianDualIP


#=============================================================================
# INC_DIR_USER is the list of all include path needed by your sources
#   - for a device server, tango dependencies are automatically appended
#   - '-I ../include' and '-I .' are automatically appended in all cases
#
INC_DIR_USER= -I . \
              -I $(SERIAL_HOME)\
              -I $(VARIANDUALCTRL_HOME)\
              -I $(VARIANDUALIP_HOME)

#=============================================================================
# LIB_DIR_USER is the list of user library directories
#   - for a device server, tango libraries directories are automatically appended
#   - '-L ../lib' is automatically appended in all cases
#
LIB_DIR_USER=

#=============================================================================
# LFLAGS_USR is the list of user link flags
#   - for a device server, tango libraries directories are automatically appended
#   - '-ldl -lpthread' is automatically appended in all cases
#
# !!! ATTENTION !!!
# Be aware that the order matters. 
# For example if you must link with libA, and if libA depends itself on libB
# you must use '-lA -lB' in this order as link flags, otherwise you will get
# 'undefined reference' errors
#
#LFLAGS_USR+=


#=============================================================================
# CXXFLAGS_USR lists the compilation flags specific for your library/device/exe
# This is the place where to put your compile-time macros using '-Dmy_macro'
#
# -DACE_HAS_EXCEPTIONS -D__ACE_INLINE__ for ACE
#
#CXXFLAGS_USR+= -Wall


#=============================================================================
# TANGO_REQUIRED 
# - TRUE  : your project depends on TANGO
# - FALSE : your project does not depend on TANGO
#-----------------------------------------------------------------------------
# - NOTE : if PROJECT_TYPE is set to DEVICE, TANGO will be auto. added
#-----------------------------------------------------------------------------  
TANGO_REQUIRED = TRUE



#=============================================================================
#	include Standard TANGO compilation options
#
include $(MAKE_ENV)/tango.opt

#=============================================================================
#	POST_PROCESSING: action to be done after normal make.
#	e.g.:  change executable file name, .....
#POST_PROCESSING = \
#	mv bin/$(BIN_DIR)/$(PACKAGE_NAME) bin/$(BIN_DIR)/$(PACKAGE_NAME)_DS

#=============================================================================
# SVC_OBJS is the list of all objects needed to make the output
#
SVC_OBJS =  $(SVC_SERIAL_OBJS) \
            $(SVC_VARIANDUALCTRL_OBJS) \
            $(SVC_VARIANDUALIP_OBJS) \
            $(OBJDIR)/MultiClassesFactory.o \
            $(OBJDIR)/main.o

#------------  Object files for Serial class  ------------
SVC_SERIAL_OBJS = \
		$(OBJDIR)/Serial.o \
		$(OBJDIR)/SerialClass.o \
		$(OBJDIR)/SerialStateMachine.o

#------------  Object files for VarianDualCtrl class  ------------
SVC_VARIANDUALCTRL_OBJS = \
		$(OBJDIR)/VarianDualCtrl.o \
		$(OBJDIR)/VarianDualCtrlClass.o \
		$(OBJDIR)/VarianDualCtrlStateMachine.o \
		$(OBJDIR)/VarianRefreshThread.o \

#------------  Object files for VarianDualIP class  ------------
SVC_VARIANDUALIP_OBJS = \
		$(OBJDIR)/VarianDualIP.o \
		$(OBJDIR)/VarianDualIPClass.o \
		$(OBJDIR)/VarianDualIPStateMachine.o


#=============================================================================
#	include common targets
#
include $(MAKE_ENV)/common_target.opt


#=============================================================================
# Following are dependancies of the classes used by project
#
#------------  Object files dependancies for Serial class  ------------
SERIAL_INCLUDES = \
		$(SERIAL_HOME)/Serial.h \
		$(SERIAL_HOME)/SerialClass.h

$(OBJDIR)/Serial.o:  $(SERIAL_HOME)/Serial.cpp $(SERIAL_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/Serial.o
$(OBJDIR)/SerialClass.o:  $(SERIAL_HOME)/SerialClass.cpp $(SERIAL_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/SerialClass.o
$(OBJDIR)/SerialStateMachine.o:  $(SERIAL_HOME)/SerialStateMachine.cpp $(SERIAL_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/SerialStateMachine.o

#------------  Object files dependancies for VarianDualCtrl class  ------------
VARIANDUALCTRL_INCLUDES = \
		$(VARIANDUALCTRL_HOME)/VarianDualCtrl.h \
		$(VARIANDUALCTRL_HOME)/VarianDualCtrlClass.h \
		$(VARIANDUALCTRL_HOME)/VarianRefreshThread.h

$(OBJDIR)/VarianDualCtrl.o:  $(VARIANDUALCTRL_HOME)/VarianDualCtrl.cpp $(VARIANDUALCTRL_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/VarianDualCtrl.o
$(OBJDIR)/VarianDualCtrlClass.o:  $(VARIANDUALCTRL_HOME)/VarianDualCtrlClass.cpp $(VARIANDUALCTRL_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/VarianDualCtrlClass.o
$(OBJDIR)/VarianDualCtrlStateMachine.o:  $(VARIANDUALCTRL_HOME)/VarianDualCtrlStateMachine.cpp $(VARIANDUALCTRL_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/VarianDualCtrlStateMachine.o
$(OBJDIR)/VarianRefreshThread.o:  $(VARIANDUALCTRL_HOME)/VarianRefreshThread.cpp $(VARIANDUALCTRL_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/VarianRefreshThread.o

#------------  Object files dependancies for VarianDualIP class  ------------
VARIANDUALIP_INCLUDES = \
		$(VARIANDUALIP_HOME)/VarianDualIP.h \
		$(VARIANDUALIP_HOME)/VarianDualIPClass.h

$(OBJDIR)/VarianDualIP.o:  $(VARIANDUALIP_HOME)/VarianDualIP.cpp $(VARIANDUALIP_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/VarianDualIP.o
$(OBJDIR)/VarianDualIPClass.o:  $(VARIANDUALIP_HOME)/VarianDualIPClass.cpp $(VARIANDUALIP_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/VarianDualIPClass.o
$(OBJDIR)/VarianDualIPStateMachine.o:  $(VARIANDUALIP_HOME)/VarianDualIPStateMachine.cpp $(VARIANDUALIP_INCLUDES)
	$(CC) $(CXXFLAGS) -c $< -o $(OBJDIR)/VarianDualIPStateMachine.o


