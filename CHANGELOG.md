# Changelog


#### VarianDualIP-1.4 - 27/03/23:
     remove read control type form always_executed_hook

#### VarianDualIP-1.3 - 15/03/23:
     several modification homogenization of fixed/step mode for VacGaugeServer

#### VarianDualIP-1.1 - 30/08/19:
     Add StepMode7000 and FixMode5000 commands.
     Do not set state to moving if fix mode

#### VarianDualIP-1.0 - 04/03/19:
    Initial Revision in GIT
